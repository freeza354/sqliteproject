package com.example.db_sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    public static class DBAdapter {

        public static final String KEY_ROWID = "_id";
        public static final String KEY_ISEBN = "isbn";
        public static final String KEY_TITLE = "title";
        public static final String KEY_PUBLISHER = "publisher";
        public static final String TAG = "DBAdapter";

        private static final String DATABASE_NAME = "books";
        private static final String DATABASE_TABLE = "titles";
        private static final int DATABASE_VERSION = 1;

        private static final String DATABASE_CREATE = " create table titles (_id integer primary key autoincrement, "
                + "isbn text not null, title text not null," + "publisher text not null);";

        private final Context context;
        private DatabaseHelper DBHelper;
        private SQLiteDatabase db;

        public DBAdapter(Context ctx) {
            this.context = ctx;
            DBHelper = new DatabaseHelper(context);
        }

        private static class DatabaseHelper extends SQLiteOpenHelper {
            DatabaseHelper(Context context) {
                super(context, DATABASE_NAME, null, DATABASE_VERSION);
            }

            public void onCreate(SQLiteDatabase db) {
                db.execSQL(DATABASE_CREATE);
            }

            public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
                Log.w(TAG, "Upgrading database from version : "
                        + oldVer + " to : " + newVer
                        + ", which will destroy all old data.");
                db.execSQL("Drop table if exists titles");
                onCreate(db);
            }

        }

        public DBAdapter open() throws SQLException {
            db = DBHelper.getWritableDatabase();
            return this;
        }

        public void close() {
            DBHelper.close();
        }

        public long insertTitles(String isbn, String title, String publisher) {
            ContentValues initVal = new ContentValues();
            initVal.put(KEY_ISEBN, isbn);
            initVal.put(KEY_TITLE, title);
            initVal.put(KEY_PUBLISHER, publisher);
            return db.insert(DATABASE_TABLE, null, initVal);
        }

        public boolean delete(long rowid) {
            return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowid, null) > 0;
        }

        public Cursor getAllTitles() {
            return db.query(DATABASE_TABLE, new String[]{
                            KEY_ROWID,
                            KEY_ISEBN,
                            KEY_TITLE,
                            KEY_PUBLISHER},
                    null, null, null, null, null);
        }

        public Cursor getTitle(long rowId) throws SQLException {
            Cursor mCursor =
                    db.query(true, DATABASE_TABLE, new String[]{
                                    KEY_ROWID,
                                    KEY_ISEBN,
                                    KEY_TITLE,
                                    KEY_PUBLISHER},
                            KEY_ROWID + "=" + rowId,
                            null, null, null, null, null);

            if (mCursor != null) {
                mCursor.moveToFirst();
            }
            return mCursor;
        }

        public boolean updateTitle(long rowId, String isbn, String title, String publisher) {
            ContentValues args = new ContentValues();
            args.put(KEY_ISEBN, isbn);
            args.put(KEY_TITLE, title);
            args.put(KEY_PUBLISHER, publisher);

            return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}

